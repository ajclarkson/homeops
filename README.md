# HomeOps

*Home Infrastructure as Code*

This project contains the ansible playbook / role definitions for managing my home servers meaning that they are easily recreatable and relatively portable.

It requires `ansible` to be installed.

## Playbooks

- [init_host:](#init_host) Configure a new box with a static ip / hostname / ssh key.

### init_host

```
ansible-playbook -e init_target_host=<DHCP_ASSIGNED_IP_ADDRESS> -e init_target_static_address=<DESIRED_STATIC_IP_ADDRESS> -e init_target_hostname=<DESIRED_HOSTNAME> playbooks/init_host.yml
```

This command will initialise the host with the desired static ip address via `/etc/network/interfaces` configuration file. It will also set the hostname, and copy the current users ssh key. To perform these operations by default it will use raspberry pi credentials to log into the host, to override you can also provide `-e ansible_user=<USERNAME> -e ansible_password=<PASSWORD>`.

This playbook will trigger a restart of the host and then exit, the host will then be available on the desired IP.

Because the `init_host` playbook uses ssh password login to initialise the new host - you need `sshpass` installed. To install via brew do the following:

```
brew install http://git.io/sshpass.rb
```





# New Raspberry Pi Kube Host

- Flash image with balena etcher
- Remount and `touch /Volumes/boot/ssh` on the card
- Boot pi and find IP via router
- SSH to pi to accept remote key
- `ansible-playbook -e init_target_host=<DHCP_ASSIGNED_IP_ADDRESS> -e init_target_static_address=<DESIRED_STATIC_IP_ADDRESS> -e init_target_hostname=<DESIRED_HOSTNAME> playbooks/init_host.yml`
- (You might need to modify your known_hosts file, then re-accept the key by sshing mnaually if the IP is being reused before the next one works)
- `ansible-playbook -e init_target_host=<STATIC IP> playbooks/kube_init.yml`
NB k3sup / arkade commands to be run from local machine NOT the pi
- For server: `k3sup install --cluster --user pi --k3s-extra-args '--no-deploy traefik --write-kubeconfig-mode 664' --k3s-version v1.19.16+k3s1 --ip $SERVER `
- For secondary master: `k3sup join --user pi --server --server-user pi --k3s-version v1.19.16+k3s1 --ip $NODE_IP --server-ip $SERVER_IP` 
- For worker: `k3sup join --user pi --k3s-version v1.19.16+k3s1 --ip $WORKER_IP --server-ip $SERVER_IP`


# dashboard
- curl -sLS https://get.arkade.dev | sudo sh
- arkade install kubernetes-dashboard (and follow instructions for creating service account)

# nfs-server

- `sudo mkdir /data`
- `sudo mount /dev/sda1 /data` and check you have the right partition

# LB
- `kubectl create namespace kube-ingress`
- `helm install --namespace kube-ingress metallb stable/metallb`
- `kubectl apply -f kubernetes-manifests/metallb-config.yml`
- `arkade install nginx-ingress --set spec.loadBalancerIP=10.0.0.201`

# cert

- `arkade install cert-manager`
- `kubectl apply -f kubernetes-manifests/cluster-issuer.yml`
