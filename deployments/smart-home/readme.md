# Smart Home Setup

The smart home namespace uses helm charts backed by the dynamic nfs provisioner for persistence.

## Smart Home Namespace

All of the deployments are grouped under the `smart-home` namespace for easy discovery

```
kubectl create namespace smart-home
```

## Mosquitto

```
helm repo add halkeye https://halkeye.github.io/helm-charts
helm upgrade -i -n smart-home mqtt -f deployments/smart-home/mqtt-values.yaml halkeye/mosquitto
```

## Home Assistant

```
helm repo add billimek https://billimek.com/billimek-charts/
helm upgrade -i -n smart-home home-assistant -f deployments/smart-home/home-assistant-values.yaml billimek/home-assistant
```


## Node Red

```
helm repo add billimek https://billimek.com/billimek-charts/
helm upgrade -i -n smart-home node-red -f deployments/smart-home/node-red-values.yaml billimek/node-red
```

If setting up a new installation the following add ons should be added to pallate:

- `node-red-contrib-bigtimer`
- `node-red-contrib-home-assistant-websocket`
- `node-red-contrib-stoptimer`
- `node-red-contrib-time-range-switch`
- `node-red-contrib-fsm`
- `node-red-contrib-holiday`
- `node-red-contrib-state-machine`

## Hue MQTT Bridge

```
kubectl apply -f deployments/smart-home/hue-mqtt-bridge.yaml -n smart-home
```

In the resulting `PVC` a file called `config.json` is required with the format:

```
{
  "broker": {
    "host": "mqtt.kube.local"
  },
  "bridges": [
    {
      "host": "XXXXX",
      "username": "XXXX",
      "interval": 500,
      "prefix": "hue"
    }
  ]
}
```