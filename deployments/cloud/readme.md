# Cloud Setup

The cloud namespace contains cloud services that provide backup/storage such as nextcloud.

## Cloud Namespace

All of the deployments are grouped under the `cloud` namespace for easy discovery

```
kubectl create namespace cloud
```

## Nextcloud

### Local Storage

For performance reasons the nextcloud pod is configured to use a local volume. This requires a storage class and a persistent volume to be setup.

There is a custom storage class to ensure only this persistent volume will serve the nextcloud claim.

First create the class:

```
kubectl apply -f nextcloud-class.yaml
```

Then the persistent volume:

```
kubectl apply -f nextcloud-pv.yaml
```

### Deployment

```
helm upgrade -i -n cloud nextcloud -f nextcloud-values.yaml stable/nextcloud
```

In the `config.php` file make sure the following are set:

```
'overwrite.cli.url' => 'cloud.clarksons.me',
'overwriteprotocol' => 'https',
'overwritehost' => 'cloud.clarksons.me'
```