# Monitoring Setup

The monitoring namespace uses the TICK stack and should be created using a mix of helm charts and custom configuration of some local storage options for the influxdb pod.

To use the influxdata helm charts, the repository must be added:

```
helm repo add influxdata https://helm.influxdata.com/
```

## Monitoring Namespace

All of the deployments are grouped under the `monitoring` namespace for easy discovery

```
kubectl create namespace monitoring
```

## InfluxDB

### Local Storage

For performance reasons, the influxdb pod is configured to use a local volume. This requires a storage class and a persistent volume to be setup.

There is a custom storage class to ensure only this persistent volume will serve the influxdb claim.

First create the class:

```
kubectl apply -f deployments/monitoring/influxdb-class.yaml
```
Then create the persistent volume:

```
kubectl apply -f deployments/monitoring/influxdb-pv.yaml
```

### Deployment

When the persistence is setup, influxdb is installed using the helm chart:

```
helm upgrade -i -n monitoring monitoring-influxdb -f deployments/monitoring/influxdb-values.yaml influxdata/influxdb
```

## Chronograf

**N.B** The NFS dynamic provisioner must already exist in the cluster

```
helm upgrade -i -n monitoring monitoring-chronograf -f deployments/monitoring/chronograf-values.yaml influxdata/chronograf
```

## Kapacitor

```
helm upgrade -i -n monitoring monitoring-kapacitor -f deployments/monitoring/kapacitor-values.yaml influxdata/kapacitor
```