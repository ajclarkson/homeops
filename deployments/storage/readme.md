# Storage

## Dynamic NFS Provisioning

To allow for pods using the storage class `nfs-client` to automatically have PV / PVC created for them 

```
helm upgrade -i --set nfs.server=<NFS_IP_ADDRESS> --set nfs.path=<NFS_PATH> nfs-client-provisioner -f deployments/storage/nfs-client-provisioner-values.yaml stable/nfs-client-provisioner
```

## MongoDB

```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm upgrade -i mongodb -f mongodb-values.yaml bitnami/mongodb
```